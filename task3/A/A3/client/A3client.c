#include "exp1.h"
#include "exp1lib.h"

void file_copy(const char* host, const char* file_path, int bufsize) {
	int sock;
	char buf[bufsize];
	FILE* fp;
	int ret;
	int n = 0;

	struct hostent* hp;
	struct sockaddr_in addr;

	sock = socket(PF_INET, SOCK_DGRAM, 0);
	if ( sock < 0 ) {
		perror("sock");
		exit(-1);
	}
	memset((char*)&addr, 0, sizeof(addr));
	addr.sin_family = PF_INET;
	addr.sin_port = htons(11111);
	hp = gethostbyname(host);
	memcpy((void*)&addr.sin_addr, hp->h_addr_list[0], hp->h_length);

	sock = exp1_udp_connect(host, 11111);
	fp = fopen(file_path, "r");
	ret = fread(buf, sizeof(char), bufsize, fp);
	while(ret > 0) {
		send(sock, buf, ret, 0);
		ret = fread(buf, sizeof(char), bufsize, fp);
		n++;
		if ( n % 100 == 0 ) {
			sleep(1);
		}
	}
	close(sock);

	printf("%d packets sent.\n", n);
}

int main(int argc, char** argv) {

	double start = 0;
	double end = 0;
	double diff = 0;

	if(argc != 4) {
		printf("usage: %s [ip address] [filename] [bufsize]\n", argv[0]);
		exit(-1);
	}

	int bufsize = atoi(argv[3]);
	start = gettimeofday_sec();
	file_copy(argv[1], argv[2], bufsize);
	end = gettimeofday_sec();
	diff = end - start;
	printf("bufsize: %d, overhead is %10.10f\n", bufsize, diff);

	return 0;
}

