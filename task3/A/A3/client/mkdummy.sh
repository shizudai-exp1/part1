#!/bin/sh

dd if=/dev/zero of=testdata/dummy1M bs=1M count=1
dd if=/dev/zero of=testdata/dummy5M bs=1M count=5
dd if=/dev/zero of=testdata/dummy10M bs=1M count=10
dd if=/dev/zero of=testdata/dummy25M bs=1M count=25
dd if=/dev/zero of=testdata/dummy50M bs=1M count=50
