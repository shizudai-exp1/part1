#include "exp1.h"
#include "exp1lib.h"

#define BUFSIZE 102400000

char buf[BUFSIZE];
int main(int argc, char** argv) {
	int sock_listen;
	int ret;
	FILE* fp;
	int n=0;

	sock_listen = exp1_udp_listen(11111);
	fp = fopen("tmp.txt", "w");
	ret = recv(sock_listen, buf, BUFSIZE, 0);
	n++;
	fprintf(stderr, "%d packets received.\n", n);
	while (ret > 0) {
		fwrite(buf, sizeof(char), ret, fp);
		fflush(fp);
		ret = recv(sock_listen, buf, BUFSIZE, 0);
		n++;
		fprintf(stderr, "%d packets received.\n", n);
	}
	close(sock_listen);
	fclose(fp);

	return 0;
}

