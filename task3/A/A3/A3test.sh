#!/bin/bash

A3SERVERDIR=server
A3SERVER=A3server
A3CLIENTDIR=client
A3CLIENT=A3client
SERVERIP=localhost
TESTDATADIR=client/testdata/
TESTDATA=`find ${TESTDATADIR} | grep dummy | sort -n`
TESTBUFFSIZE="64 128 256 512 1024 2048 4096"
RESULT=server/tmp.txt

#for i in `echo ${TESTBUFFSIZE}` ; do
#done

for j in `echo ${TESTDATA}` ; do
	echo ${j}
	if [ -f ${RESULT} ] ; then
		rm ${RESULT}
	fi
	cd ${A3SERVERDIR}
	./${A3SERVER} &
	cd -
	${A3CLIENTDIR}/${A3CLIENT} ${SERVERIP} ${j} 16
	A3SERVERPID=`ps -ef | grep A3server | awk '{print $2}'`
#	kill ${A3SERVERPID}

	diff ${j} ${RESULT}
done

