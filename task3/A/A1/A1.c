#include "exp1.h"
#include "exp1lib.h"

void file_copy_rw(char* file_path, int bufsize) {
	int fpr;
	int fpw;
	int ret;
	char buf[bufsize];

	fpr = open(file_path, O_RDONLY);
	fpw = open("tmp.txt", O_WRONLY);

	ret = read(fpr, buf, sizeof(char)*bufsize);
	while (ret > 0) {
		write(fpw, buf, sizeof(char)*ret);
		ret = read(fpr, buf, sizeof(char)*bufsize);
	}
	close(fpr);
	close(fpw);
}

void file_copy_frw(char* file_path, int bufsize) {
	FILE* fpr;
	FILE* fpw;
	int ret;
	char buf[bufsize];

	fpr = fopen(file_path, "r");
	fpw = fopen("tmp.txt", "w");

	// Nobuffer (in f function)
//	setvbuf(fpr, NULL, _IONBF, 0);
//	setvbuf(fpw, NULL, _IONBF, 0);

	ret = fread(buf, sizeof(char), bufsize, fpr);
	while (ret > 0) {
		fwrite(buf, sizeof(char), ret, fpw);
		ret = fread(buf, sizeof(char), bufsize, fpr);
	}
	fclose(fpr);
	fclose(fpw);
}

int main(int argc, char** argv) {

	double start = 0;
	double end = 0;
	double diff = 0;
	uint16_t buf_size[] = {4, 8, 16, 32, 64, 128};

	if(argc != 2) {
		printf("usage: %s [filename]\n", argv[0]);
		exit(-1);
	}

	printf("READ/WRITE\n");
	for ( int i=0; i<sizeof(buf_size)/sizeof(uint16_t); i++ ) {
		start = gettimeofday_sec();
		file_copy_rw(argv[1], buf_size[i]);
		end = gettimeofday_sec();
		diff = end - start;
		printf("bufsize: %d, overhead is %10.10f\n", buf_size[i], diff);
	}

	printf("FREAD/FWRITE\n");
	for ( int i=0; i<sizeof(buf_size)/sizeof(uint16_t); i++ ) {
		start = gettimeofday_sec();
		file_copy_frw(argv[1], buf_size[i]);
		end = gettimeofday_sec();
		diff = end - start;
		printf("bufsize: %d, overhead is %10.10f\n", buf_size[i], diff);
	}

	return 0;
}

