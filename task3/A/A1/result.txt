READ/WRITE
bufsize: 4, overhead is 8.2343890667
bufsize: 8, overhead is 3.8839180470
bufsize: 16, overhead is 1.9538002014
bufsize: 32, overhead is 0.9864549637
bufsize: 64, overhead is 0.4990749359
bufsize: 128, overhead is 0.2492110729
FREAD/FWRITE
bufsize: 4, overhead is 0.1422300339
bufsize: 8, overhead is 0.0885939598
bufsize: 16, overhead is 0.0626590252
bufsize: 32, overhead is 0.0482821465
bufsize: 64, overhead is 0.0414490700
bufsize: 128, overhead is 0.0388009548
