#!/bin/bash

SERVERDIR=server
SERVER=A2server
CLIENTDIR=client
CLIENT=A2client
SERVERIP=localhost
TESTDATADIR=client/testdata/
TESTDATA=client/testdata/dummy500M
TESTBUFFSIZE="64 128 256 512 1024 2048 4096 8192 16384 32768 65536 131072 262144 524288 1048576"
RESULT=tmp.txt

for i in `echo ${TESTBUFFSIZE}` ; do
	echo "BUFFSIZE: ${i}"
	if [ -f ${RESULT} ] ; then
		rm ${RESULT}
	fi
	cd ${SERVERDIR}
	./${SERVER} ${i} &
	sleep 5s
	cd -
	${CLIENTDIR}/${CLIENT} ${SERVERIP} ${TESTDATA} ${i}
	sleep 5s
done

