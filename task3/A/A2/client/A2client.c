#include "exp1.h"
#include "exp1lib.h"

void file_copy(const char* host, const char* file_path, int bufsize) {
	int sock;
	char buf[bufsize];
	FILE* fp;
	int ret;

	double start = 0;
	double end = 0;
	double diff = 0;

	sock = exp1_tcp_connect(host, 11111);
	fp = fopen(file_path, "r");
	ret = fread(buf, sizeof(char), bufsize, fp);
	while(ret > 0) {
		start = gettimeofday_sec();
		send(sock, buf, ret, 0);
//		write(sock, buf, ret);
		end = gettimeofday_sec();
		diff = diff + (end - start);
		ret = fread(buf, sizeof(char), bufsize, fp);
	}
	close(sock);
	printf("bufsize: %d, overhead is %10.10f\n", bufsize, diff);
}

int main(int argc, char** argv) {

	if(argc != 4) {
		printf("usage: %s [ip address] [filename] [bufsize]\n", argv[0]);
		exit(-1);
	}

	int bufsize = atoi(argv[3]);
	file_copy(argv[1], argv[2], bufsize);

	return 0;
}

