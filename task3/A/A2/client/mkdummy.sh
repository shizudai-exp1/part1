#!/bin/sh

if [ ! -d testdata ] ; then
	mkdir testdata
fi

dd if=/dev/zero of=testdata/dummy1M bs=1M count=1
dd if=/dev/zero of=testdata/dummy5M bs=1M count=5
dd if=/dev/zero of=testdata/dummy10M bs=1M count=10
dd if=/dev/zero of=testdata/dummy25M bs=1M count=25
dd if=/dev/zero of=testdata/dummy50M bs=1M count=50
dd if=/dev/zero of=testdata/dummy75M bs=1M count=75
dd if=/dev/zero of=testdata/dummy100M bs=1M count=100
dd if=/dev/zero of=testdata/dummy200M bs=1M count=200
dd if=/dev/zero of=testdata/dummy300M bs=1M count=300
dd if=/dev/zero of=testdata/dummy400M bs=1M count=400
dd if=/dev/zero of=testdata/dummy500M bs=1M count=500

