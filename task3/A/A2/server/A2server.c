#include "exp1.h"
#include "exp1lib.h"

int main(int argc, char** argv) {
	int sock_listen;
	int sock_client;
	struct sockaddr addr;
	int len;
	int ret;
	FILE* fp;

	if ( argc != 2 ) {
		printf("Usage: %s: A2server bufsize\n", argv[0]);
		exit(-1);
	}
	int bufsize = atoi(argv[1]);
	char buf[bufsize];

	sock_listen = exp1_tcp_listen(11111);
	sock_client = accept(sock_listen, &addr, (socklen_t*) &len);

	fp = fopen("tmp.txt", "w");
//	ret = recv(sock_client, buf, bufsize, 0);
	ret = read(sock_client, buf, bufsize);
	while (ret > 0) {
		fwrite(buf, sizeof(char), ret, fp);
//		ret = recv(sock_client, buf, bufsize, 0);
		ret = read(sock_client, buf, bufsize);
	}
	close(sock_client);
	close(sock_listen);

	return 0;
}

