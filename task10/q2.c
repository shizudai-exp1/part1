#include "../exp1lib/exp1.h"

void* exp1_thread(void *param){
  int* psock;
  int sock;

  // in general, thread resource is not released when the thread will be finished.
  // pthread_detach enable the thread to release resource [myself] when the thread will be finished.
  // pthread_self returns pid of the thread
  // however, it disable this thread to be thread_join from othre thread.
  pthread_detach(pthread_self());
  psock = (int*) param;
  sock = *psock;
  free(psock);
  
  sleep(5);
  
  printf("thread %d is ending\n", sock);
}

void exp1_create_thread(int sock)
{
  int *psock;
  pthread_t th;
  psock = malloc(sizeof(int));
  *psock = sock;

  pthread_create(&th, NULL, exp1_thread, psock);
  printf("thread %d is created\n", sock);
}


int main(int argc, char **argv)
{
  int i = 0;

  while(1){
    exp1_create_thread(i);
    i++;
    sleep(1);
  }
}
