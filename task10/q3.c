#include "../exp1lib/exp1.h"

int main(int argc, char **argv)
{
  while(1){
    int pid = fork();

    if(pid == 0){
      // Child Process
      sleep(5);
      printf("hello from child: pid = %d\n", getpid());
      _exit(0);
    }else{
      // Parent Process
      sleep(1);
      printf("pid = %d created\n", pid);
    }
    sleep(1);
  }
}

