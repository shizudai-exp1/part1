#include "../exp1lib/exp1.h"

void test_func(char *msg)
{
  uint32_t y = 0x87654321;
  char buf[16] = "abcdefghijklmno";
  uint32_t z = 0xcba98765;
  int i = 0;
  int j = 0;
  uint64_t base = 0xEEEEEEEEEEEEEEEE;
  uint64_t th = 0xFFFFFFFFFFFFFFFF;

  printf("%p: buf\n", buf);
  printf("%p: &y\n", &y);
  printf("%p: msg\n", msg);
  printf("%p: &msg\n", &msg);
  printf("%p: &z\n", &z);
  printf("%p: &i\n", &i);
  printf("%p: &j\n", &j);
  printf("%p: &base\n", &base);
  printf("%p: &th\n", &th);

  printf("\n");
  printf("start dump memory\n");
  base = (uint64_t)&z;
  base -= 128;
  for(i = 0; i < 64; i++){
    printf("%p: ", base);
    for(j = 0; j < 8; j++){
      uint8_t *p = (uint8_t*) base;
      printf("%02x", *(p + j));
    }
    printf("\n");
    base += 8;
  }
  printf("end dump memory\n");
  printf("\n");

  // msg:char data[] to buf:char[16] 
  strcpy(buf, msg);
}

int main(int argc, char** argv) {
  uint32_t x = 0x44332211;
  char data[] = "abcd" "efgh" "ijkl" "mnop" "qrst" "uvwx"
                "abcd" "efgh" "ijkl" "mnop" "qrst" "uvwx";
  printf("%p: data\n", data);
  printf("%p: &x\n", &x);
  printf("%p: main\n", main);
  printf("%p: &argc\n", &argc);
  printf("%p: &argv\n", &argv);

  test_func(data);
}

