#include "../exp1lib/exp1.h"

uint64_t testFunc( uint64_t p1, uint64_t p2 ) {
	uint64_t x1 = 0x2222222222222222;
	uint64_t x2 = 0x3333333333333333;
	return 0xAAAAAAAAAAAAAAAA;
}


int main( int argc, char** argv ) {
	uint64_t p1 = 0x1111111111111111;
	uint64_t p2 = 0xffffffffffffffff;
	uint64_t v = testFunc(p1, p2);
}

