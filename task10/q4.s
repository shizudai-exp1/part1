	.file	"q4.c"
	.section	.rodata
.LC0:
	.string	"%p: buf\n"
.LC1:
	.string	"%p: &y\n"
.LC2:
	.string	"%p: msg\n"
.LC3:
	.string	"%p: &msg\n"
.LC4:
	.string	"%p: &z\n"
.LC5:
	.string	"%p: &i\n"
.LC6:
	.string	"%p: &j\n"
.LC7:
	.string	"%p: &base\n"
.LC8:
	.string	"%p: &th\n"
.LC9:
	.string	"start dump memory"
.LC10:
	.string	"%p: "
.LC11:
	.string	"%02x"
.LC12:
	.string	"end dump memory"
	.text
	.globl	test_func
	.type	test_func, @function
test_func:
.LFB2:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$80, %rsp
	movq	%rdi, -72(%rbp)
	movl	$-2023406815, -12(%rbp)
	movabsq	$7523094288207667809, %rax
	movq	%rax, -32(%rbp)
	movabsq	$31365138664352361, %rax
	movq	%rax, -24(%rbp)
	movl	$-878082203, -36(%rbp)
	movl	$0, -40(%rbp)
	movl	$0, -44(%rbp)
	movabsq	$-1229782938247303442, %rax
	movq	%rax, -56(%rbp)
	movq	$-1, -64(%rbp)
	leaq	-32(%rbp), %rax
	movq	%rax, %rsi
	movl	$.LC0, %edi
	movl	$0, %eax
	call	printf
	leaq	-12(%rbp), %rax
	movq	%rax, %rsi
	movl	$.LC1, %edi
	movl	$0, %eax
	call	printf
	movq	-72(%rbp), %rax
	movq	%rax, %rsi
	movl	$.LC2, %edi
	movl	$0, %eax
	call	printf
	leaq	-72(%rbp), %rax
	movq	%rax, %rsi
	movl	$.LC3, %edi
	movl	$0, %eax
	call	printf
	leaq	-36(%rbp), %rax
	movq	%rax, %rsi
	movl	$.LC4, %edi
	movl	$0, %eax
	call	printf
	leaq	-40(%rbp), %rax
	movq	%rax, %rsi
	movl	$.LC5, %edi
	movl	$0, %eax
	call	printf
	leaq	-44(%rbp), %rax
	movq	%rax, %rsi
	movl	$.LC6, %edi
	movl	$0, %eax
	call	printf
	leaq	-56(%rbp), %rax
	movq	%rax, %rsi
	movl	$.LC7, %edi
	movl	$0, %eax
	call	printf
	leaq	-64(%rbp), %rax
	movq	%rax, %rsi
	movl	$.LC8, %edi
	movl	$0, %eax
	call	printf
	movl	$10, %edi
	call	putchar
	movl	$.LC9, %edi
	call	puts
	leaq	-36(%rbp), %rax
	movq	%rax, -56(%rbp)
	movq	-56(%rbp), %rax
	addq	$-128, %rax
	movq	%rax, -56(%rbp)
	movl	$0, -40(%rbp)
	jmp	.L2
.L5:
	movq	-56(%rbp), %rax
	movq	%rax, %rsi
	movl	$.LC10, %edi
	movl	$0, %eax
	call	printf
	movl	$0, -44(%rbp)
	jmp	.L3
.L4:
	movq	-56(%rbp), %rax
	movq	%rax, -8(%rbp)
	movl	-44(%rbp), %eax
	movslq	%eax, %rdx
	movq	-8(%rbp), %rax
	addq	%rdx, %rax
	movzbl	(%rax), %eax
	movzbl	%al, %eax
	movl	%eax, %esi
	movl	$.LC11, %edi
	movl	$0, %eax
	call	printf
	movl	-44(%rbp), %eax
	addl	$1, %eax
	movl	%eax, -44(%rbp)
.L3:
	movl	-44(%rbp), %eax
	cmpl	$7, %eax
	jle	.L4
	movl	$10, %edi
	call	putchar
	movq	-56(%rbp), %rax
	addq	$8, %rax
	movq	%rax, -56(%rbp)
	movl	-40(%rbp), %eax
	addl	$1, %eax
	movl	%eax, -40(%rbp)
.L2:
	movl	-40(%rbp), %eax
	cmpl	$63, %eax
	jle	.L5
	movl	$.LC12, %edi
	call	puts
	movl	$10, %edi
	call	putchar
	movq	-72(%rbp), %rdx
	leaq	-32(%rbp), %rax
	movq	%rdx, %rsi
	movq	%rax, %rdi
	call	strcpy
	leave
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE2:
	.size	test_func, .-test_func
	.section	.rodata
.LC13:
	.string	"%p: data\n"
.LC14:
	.string	"%p: &x\n"
.LC15:
	.string	"%p: main\n"
.LC16:
	.string	"%p: &argc\n"
.LC17:
	.string	"%p: &argv\n"
	.text
	.globl	main
	.type	main, @function
main:
.LFB3:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$80, %rsp
	movl	%edi, -68(%rbp)
	movq	%rsi, -80(%rbp)
	movl	$1144201745, -4(%rbp)
	movabsq	$7523094288207667809, %rax
	movq	%rax, -64(%rbp)
	movabsq	$8101815670912281193, %rax
	movq	%rax, -56(%rbp)
	movabsq	$8680537053616894577, %rax
	movq	%rax, -48(%rbp)
	movabsq	$7523094288207667809, %rax
	movq	%rax, -40(%rbp)
	movabsq	$8101815670912281193, %rax
	movq	%rax, -32(%rbp)
	movabsq	$8680537053616894577, %rax
	movq	%rax, -24(%rbp)
	movb	$0, -16(%rbp)
	leaq	-64(%rbp), %rax
	movq	%rax, %rsi
	movl	$.LC13, %edi
	movl	$0, %eax
	call	printf
	leaq	-4(%rbp), %rax
	movq	%rax, %rsi
	movl	$.LC14, %edi
	movl	$0, %eax
	call	printf
	movl	$main, %esi
	movl	$.LC15, %edi
	movl	$0, %eax
	call	printf
	leaq	-68(%rbp), %rax
	movq	%rax, %rsi
	movl	$.LC16, %edi
	movl	$0, %eax
	call	printf
	leaq	-80(%rbp), %rax
	movq	%rax, %rsi
	movl	$.LC17, %edi
	movl	$0, %eax
	call	printf
	leaq	-64(%rbp), %rax
	movq	%rax, %rdi
	call	test_func
	leave
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE3:
	.size	main, .-main
	.ident	"GCC: (Debian 4.9.2-10) 4.9.2"
	.section	.note.GNU-stack,"",@progbits
