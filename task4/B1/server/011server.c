#include "exp1.h"
#include "exp1lib.h"
#include <stdbool.h>

int main(int argc, char** argv) {

	int sock_listen;
	int sock_client;
	struct sockaddr addr;
	socklen_t len = sizeof(addr);

	sock_listen = exp1_tcp_listen(11111);

	while(true) {
		int ret;
		sock_client = accept(sock_listen, (struct sockaddr*)&addr, &len);
		ret = 1;
		while ( ret == 1 ) {
			ret = exp1_do_talk(sock_client);
		}
		close(sock_client);
	}

	return 0;
}

