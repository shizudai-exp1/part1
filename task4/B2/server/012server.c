#include "exp1.h"
#include "exp1lib.h"
#include <sys/select.h>
#include <stdbool.h>

void exp1_init_clients();
void exp1_set_fds(fd_set* pfds, int accept_sock);
void exp1_add(int sock);
void exp1_remove(int id);
int exp1_get_max_sock();
int exp1_broadcast(char* buf, int size, int from);
void exp1_do_server(int sock_listen);

int main(int argc, char** argv) {

	int sock_listen;

	exp1_init_clients();
	sock_listen = exp1_tcp_listen(11111);

	while(true) {
		exp1_do_server(sock_listen);
	}
	close(sock_listen);
	return 0;
}

#define MAX_CLIENTS 10
int g_clients[MAX_CLIENTS];

void exp1_init_clients() {
	int i;

	for (i = 0; i < MAX_CLIENTS; i++) {
		g_clients[i] = 0;
	}
}

void exp1_set_fds(fd_set* pfds, int accept_sock) {
	int i;

	FD_ZERO(pfds);
	FD_SET(accept_sock, pfds);

	for (i = 0; i < MAX_CLIENTS; i++) {
		if (g_clients[i] == 1) {
			FD_SET(i, pfds);
		}
	}
}

void exp1_add(int sock) {
	if (sock < MAX_CLIENTS) {
		g_clients[sock] = 1;
	} else {
		printf("connection overflow\n");
		exit(-1);
	}
}

void exp1_remove(int id) {
	g_clients[id] = 0;
}

int exp1_get_max_sock() {
	int i;
	int max_sock = 0;

	for (i = 0; i < MAX_CLIENTS; i++) {
		if (g_clients[i] == 1) {
			max_sock = i;
		}
	}

	return max_sock;
}

int exp1_broadcast(char* buf, int size, int from) {
	int i;

	for (i = 0; i < MAX_CLIENTS; i++) {
		if (i == from) {
			continue;
		}

		if (g_clients[i] == 1) {
			write(i, buf, size);
		}
	}
	return 0;
}

void exp1_do_server(int sock_listen) {
	fd_set fds;
	struct sockaddr addr;
	socklen_t len = sizeof(addr);
	int max_sock;
	int i;

	exp1_set_fds(&fds, sock_listen);
	max_sock = exp1_get_max_sock();

	if (max_sock < sock_listen) {
		max_sock = sock_listen;
	}

	select(max_sock + 1, &fds, NULL, NULL, NULL);

	if (FD_ISSET(sock_listen, &fds) != 0) {
		int sock;
		sock = accept(sock_listen, (struct sockaddr *) &addr, &len);
		exp1_add(sock);
	}

	for (i = 0; i < MAX_CLIENTS; i++) {
		if (g_clients[i] == 0) {
			continue;
		}

		if (FD_ISSET(i, &fds) != 0) {
			char buf[1024];
			int ret = recv(i, buf, 1024, 0);
			if (ret > 0) {
				write(1, buf, ret);
				exp1_broadcast(buf, ret, i);
			} else {
				exp1_remove(i);
			}
		}
	}
}
